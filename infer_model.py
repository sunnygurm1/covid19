import torch
from torch import Tensor
from torch import nn

import torchvision
from typing import Optional
from torchvision import transforms
import PIL
import pydicom

import numpy as np
from pydicom.pixel_data_handlers.util import apply_color_lut, convert_color_space

#Model classifier info:
MODEL_PREDICTIONS = ['covid', 'no_finding', 'opacity']

#CPU for inference
device = torch.device("cpu")

class Flatten(nn.Module):
    "Flatten `x` to a single dimension, often used at the end of a model. `full` for rank-1 tensor"
    def __init__(self, full:bool=False):
        super().__init__()
        self.full = full

    def forward(self, x):
        return x.view(-1) if self.full else x.view(x.size(0), -1)

class AdaptiveConcatPool2d(nn.Module):
    "Layer that concats `AdaptiveAvgPool2d` and `AdaptiveMaxPool2d`." # from pytorch
    def __init__(self, sz:Optional[int]=None): 
        "Output will be 2*sz or 2 if sz is None"
        super().__init__()
        self.output_size = sz or 1
        self.ap = nn.AdaptiveAvgPool2d(self.output_size)
        self.mp = nn.AdaptiveMaxPool2d(self.output_size)
    def forward(self, x): return torch.cat([self.mp(x), self.ap(x)], 1)
    
def myhead(nf, nc):
    '''
    Inputs: nf=  # of in_features in the 4th layer , nc= # of classes
    '''
    return \
    nn.Sequential(        # the dropout is needed otherwise you cannot load the weights
            AdaptiveConcatPool2d(),
            Flatten(),
            nn.BatchNorm1d(nf,eps=1e-05,momentum=0.1,affine=True,track_running_stats=True),
            nn.Dropout(p=0.25,inplace=False),
            nn.Linear(nf, 512,bias=True),
            nn.ReLU(inplace=True),
            nn.BatchNorm1d(512,eps=1e-05,momentum=0.1,affine=True,track_running_stats=True),
            nn.Dropout(p=0.5,inplace=False),
            nn.Linear(512, nc,bias=True),
        )


my_model=torchvision.models.resnet34() 
modules=list(my_model.children())
modules.pop(-1) 
modules.pop(-1) 
temp=nn.Sequential(nn.Sequential(*modules))
tempchildren=list(temp.children()) 

#append the special fastai head
#Configured according to Model Architecture

tempchildren.append(myhead(1024,3))
model_r34=nn.Sequential(*tempchildren)

#LOAD MODEL
state = torch.load('corona_resnet34.pth',map_location=torch.device('cpu'))
model_r34.load_state_dict(state['model'])


#important to set to evaluation mode
model_r34.eval()


test_transforms = transforms.Compose([
    transforms.Resize(512),
    transforms.CenterCrop(512),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406],
                    std=[0.229, 0.224, 0.225])
])

def predict_image(model,image):
    softmaxer = torch.nn.Softmax(dim=1)
    image_tensor = PIL.Image.fromarray(image)
    #image_tensor = image_tensor.convert('RGB')
    image_tensor = test_transforms(image_tensor).float()
    image_tensor=image_tensor.unsqueeze(0)

    #convert evaluation to probabilities with softmax
    with torch.no_grad():
      processed=softmaxer(model(image_tensor))
      prob, ind = processed[0].max(0)
      # return the probability as well as the model label
    return (prob.tolist(), MODEL_PREDICTIONS[ind.tolist()])

def _get_pixels(dicom_file):
    pixels = dicom_file.pixel_array
    print('dicom file type')
    print(dicom_file.PhotometricInterpretation)
    if dicom_file.PhotometricInterpretation == 'PALETTE COLOR':
        pixels = apply_color_lut(pixels, dicom_file)
    elif dicom_file.PhotometricInterpretation == 'YBR_FULL_422' or dicom_file.PhotometricInterpretation == 'YBR_FULL':
        pixels = convert_color_space(pixels, dicom_file.PhotometricInterpretation, 'RGB')
        dicom_file.PhotometricInterpretation = 'RGB'
    elif len(pixels.shape) == 2 and (dicom_file.PhotometricInterpretation == 'MONOCHROME1' 
        or dicom_file.PhotometricInterpretation == 'MONOCHROME2'):
        pixels = np.stack((pixels, pixels, pixels), axis=2)

    # handle different dtypes
    if pixels.dtype == np.uint16:
        pixels = (pixels * (256.0 / pixels.max())).astype(np.uint8)
    elif pixels.dtype == np.int16:
        pix_max = pixels.max()
        pix_min = pixels.min()
        pixels = ((pixels + pix_min) * (256.0 / (pix_max - pix_min))).astype(np.uint8)

    return pixels

COVID-19 AI Model <!-- omit in toc --> 
===
# Model Development

See the repository by Ayrton San Joaquin: https://github.com/ajsanjoaquin/COVID-19-Scanner/blob/master/covidtesting_notebook(Resnet34).ipynb

# Inference model integration SDK

The inference model SDK is taken from Arterys: https://github.com/arterys/inference-sdk


## Standard model outputs

### Bounding box

The Arterys SDK only accepts bounding boxes - so for a classification model, we produce a bounding box that encompasses the entire image along with a label and probability.

For example:

```json
{ "protocol_version":"1.0",
  "bounding_boxes_2d": [{ "label": "covid",
                          "probability":0.94,
                          "SOPInstanceUID": "2.25.336451217722347364678629652826931415692", 
                          "top_left": [0, 0], 
                          "bottom_right": [400, 500]
                          }]
}
```


### Build and run the mock inference service container

```bash
# Start the service
docker-compose up -d

# View the logs
docker-compose logs -f

# Test the service
curl localhost:8900/healthcheck
```

### To send an inference request to the mock inference server

The `inference-test-tool/send-inference-request` script allows you to send dicom data to the mock server and exercise it. 
To use it run from inside the `inference-test-tool` folder:

```bash
./send-inference-request.sh <arguments>
```

If you don't specify any arguments, a usage message will be shown.

The script accepts the following parameters:

```
./send-inference-request.sh [-h] [-s] [--host HOST] [-p PORT] /path/to/dicom/files
```

Parameters:
* `-h`: Print usage help
* `-s`: Use it if model is a segmentation model. Otherwise bounding box output will be assumed
* `--host` and `--port`: host and port of inference server

For example, if you have a study whose dicom files you have placed in the `<ARTERYS_SDK_ROOT>/inference-test-tool/test_queue` 
folder, you may send this study to your segmentation model listening on port 8600 on the host OS by running the following 
command in the `inference-test-tool` directory:

```bash
./send-inference-request.sh -s --host 0.0.0.0 --port 8600 ./test_queue/
```

For this to work, the folder where you have your DICOM files (`test_queue` in this case) must be a subfolder of 
`inference-test-tool` so that they will be accessible inside the docker container.

import functools
import logging
import logging.config
import os
import tempfile
import yaml
import json
import numpy
import pydicom
import infer_model

from utils.image_conversion import convert_to_nifti

from utils import tagged_logger

# ensure logging is configured before flask is initialized

with open('logging.yaml', 'r') as f:
    conf = yaml.safe_load(f.read())
    logging.config.dictConfig(conf)

logger = logging.getLogger('inference')

# pylint: disable=import-error,no-name-in-module
from gateway import Gateway
from flask import make_response


def handle_exception(e):
    logger.exception('internal server error %s', e)
    return 'internal server error', 500

def get_empty_response():
    response_json = {
        'protocol_version': '1.0',
        'parts': []
    }
    return response_json, []

def testhandler(json_input, dicom_instances, input_digest):
    transaction_logger = tagged_logger.TaggedLogger(logger)
    transaction_logger.add_tags({ 'input_hash': input_digest })
    transaction_logger.info('mock_model received json_input={}'.format(json_input))
    transaction_logger.info('dicom instances = {}'.format(dicom_instances))
    response_json={
        'protocol_version': '1.0',
        'parts': [],
        'bounding_boxes_2d': []
    }
    for dicom_inst in dicom_instances:
        img = pydicom.read_file(dicom_inst)
        img_pixels = infer_model._get_pixels(img)
        vals = infer_model.predict_image(infer_model.model_r34, img_pixels)

        response_json['bounding_boxes_2d'].append(
                        { "label": vals[1],
                        "probability": vals[0], 
                        "SOPInstanceUID": img.SOPInstanceUID,
                        "top_left": [0, 0], 
                        "bottom_right": [img_pixels.shape[1], img_pixels.shape[0]]
                        }
        )

    # response_json={ 
    #     "protocol_version":"1.0",
    #     "bounding_boxes_2d": [{ "label": vals[0],
    #                             "probability": vals[1], 
    #                             "SOPInstanceUID": img.SOPInstanceUID,
    #                             "top_left": [0, 0], 
    #                             "bottom_right": [img_pixels.shape[1], img_pixels.shape[0]]
    #                             }]
    # }

    return response_json, []


if __name__ == '__main__':
    app = Gateway(__name__)
    app.register_error_handler(Exception, handle_exception)
    app.add_inference_route('/', testhandler)
    app.run(host='0.0.0.0', port=8000, debug=True, use_reloader=True)
